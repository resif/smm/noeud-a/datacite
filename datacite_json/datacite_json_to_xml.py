import json
from datetime import datetime
from pprint import pprint
import  xml.etree.ElementTree as ET

scientist_in_file = "4G2007.datacite_scientist.json"
anode_in_file = "smm.anode.xml"
outfile_base = "4G2007"
insert_comments = True


def main():
    # Read in information
    print(f'Reading {scientist_in_file=}...', end='')
    with open(scientist_in_file, 'r') as fp:
        root = json.load(fp)
    print('Done')

    # Reformat some elements to prep for XML conversion
    root = _reformat_json_datasite(root)
    root['publicationYear'] = datetime.now().year

    # Create XML
    if insert_comments is True:
        out_filename = outfile_base + '_commented.xml'
    else:
        out_filename = outfile_base + '.xml'
    print(f'Parsing {anode_in_file=}...', end='')
    tree = ET.parse(anode_in_file)
    print('Done')
    top = tree.getroot()
    # Have to do the following instead of putting in smm.anode.xml to avoid namespace generation
    top.set('xmlns', "http://datacite.org/schema/kernel-4")

    top = evaluate_subelements(top, root, 'blah')
    print(f'Writing ouput file={out_filename}...', end='')
    tree = ET.ElementTree(top)
    ET.indent(tree)
    tree.write(out_filename, encoding='unicode',
                           xml_declaration=True)
    print('Done')


def _reformat_json_datasite(root):
    """
    For conversion to XML, which has Attributes and variables, use a specific
    language in which the key 'VALUE' encloses variables and all other at the
    same level are attributes
    """
    # title and geoLocation are lists
    root['titles'] = [root.pop('Title')]
    root['geoLocations'] = [root.pop('GeoLocation')]
    root['descriptions'] = [{'VALUE': root.pop('DescriptionAbstract'),
                           'descriptionType': 'Abstract'}]
    # Make dates into a list and add 'Issued'
    root['dates'] = []
    if 'Dates' in root:
        root['dates'].append({'dateType': 'Collected', 'VALUE': root.pop('Dates')['Collected']})
    root['dates'].append({'dateType': 'Issued', 'VALUE': ''})


    # Create dict of Affiliations
    affiliations = _make_affiliations_dict(root.pop('Affiliations'))

    # Prepare Creator
    for x in root['creators']:
        x = _prepare_key_attribs(x, 'nameIdentifier', ['nameIdentifierScheme'])
        x = _stuff_affiliations(x, affiliations)
        if x.pop('Organization', False) is True:
            x['creatorName'] = {'VALUE': x['creatorName'],
                                'nameType': 'Organizational'}

    # Stuff Identifiers into each funderIdentifier
    for x in root['fundingReferences']:
        x = _prepare_key_attribs(x, 'funderIdentifier', ['funderIdentifierType'])

    # Organize Contributors into Datacite structure
    root['contributors'] = []
    contributors = root.pop('Contributors', {})
    if 'ProjectLeader' in contributors:
        root['contributors'].append({'VALUE': contributors['ProjectLeader'],
                                     'contributorType': 'ProjectLeader'})
    for x in contributors.pop('DataCollectors', []):
        root['contributors'].append({'VALUE': x,
                                     'contributorType': 'DataCollector'})
    for x in contributors.pop('ProjectMembers', []):
        root['contributors'].append({'VALUE': x,
                                     'contributorType': 'ProjectMember'})

    # Stuff Identifers and affiliations into each Contributor
    for x in root['contributors']:
        x['VALUE'] = _stuff_affiliations(x['VALUE'], affiliations)
        x['VALUE'] = _prepare_key_attribs(x['VALUE'], 'nameIdentifier', ['nameIdentifierScheme'])

    # Stuff relatedIdentifier Title and Identifier into the right spot
    for x in root['relatedIdentifiers']:
        x['COMMENT'] = x.pop('Title', '')
        x['VALUE'] = x.pop('Identifier', '')

    return root

def _stuff_affiliations(x, affiliations):
    if 'AffiliationShortNames' in x:
        x['affiliations'] = []
        anames = [y.strip() for y in x.pop('AffiliationShortNames').split(',')]
        for aname in anames:
            if aname not in affiliations:
                raise ValueError(f'object "{x}" affiliation "{aname}" is not in {list(affiliations.keys())}')
            x['affiliations'].append(affiliations[aname])
    return x

def _prepare_key_attribs(x, key, attribs):
    """
    Args:
        x (dict): dict with possible key and associated attributes
        key (str): key for which to collect attributes
        attribs (list of str): attributes for this key
    """
    if key in x:
        x[key] = {'VALUE': x[key]}
        for attrib in attribs:
            if attrib in x:
                x[key][attrib] = x.pop(attrib)
    else:
        for attrib in attribs:
            if attrib in x:
                raise ValueError(f'object "{x}": {attrib=} specified without {key=}')
    return x

def _make_affiliations_dict(aff_dict):
    """aff_dict is the affiliations dictionary in the input file"""
    affiliations = {}
    for x in aff_dict:
        # x = x.copy()
        key = x.pop('ShortName')
        affiliations[key] = x
        affiliations[key]['VALUE'] = affiliations[key].pop('Name')
    return affiliations

def evaluate_subelements(a, elem, name, level=0):
    """
    Args:
        a (:class:`xml.etree.ElementTree.Element`): the XML element to modify
        elem (list, dict, str, int, or float): JSON element to be translated
        name (str): the name of the element above.  Only used for evaluating
            list elements
        level (int): depth in the tree.  Used because top-level lists are
            enclosed within a list element and lower-level lists are not
    """
    if isinstance(elem, list):
        # puts list elements below a top level list name. Should only do this
        # at the top level (problem with affiliations, for example)
        assert name[-1] == 's'
        name = name[:-1]
        for x in elem:
            b = ET.SubElement(a, name)  # strip s at end
            b = evaluate_subelements(b, x, name, level+1)
    elif isinstance(elem, dict):
        if 'VALUE' in elem:
            # Create an element with attributes
            if isinstance(elem['VALUE'], str):
                a.text = elem['VALUE']
            else:
                a = evaluate_subelements(a, elem['VALUE'], name, level+1)
            for k, v in elem.items():
                if k == 'COMMENT':
                    if insert_comments is True:
                        a.insert(0, ET.Comment(v))
                elif not k == 'VALUE':
                    a.set(k, v)
        else:
            for k, v in elem.items():
                if k == "COMMENT":
                    if insert_comments is True:
                        a.insert(0, ET.Comment(v))
                    continue
                if isinstance(v, list) and level > 0:
                    a = evaluate_subelements(a, v, k, level+1)
                else:
                    b = a.find(k)   # If the element exists already, add to it
                    if b is None:
                        b = ET.SubElement(a, k)
                    b = evaluate_subelements(b, v, k, level+1)
    elif isinstance(elem, str):
        a.text = elem
    elif isinstance(elem, (int, float)):
        a.text = str(elem)
    else:
        raise TypeError(f'input element {elem=} is not a list, dict, str, int or float')
    return a


if __name__ == "__main__":
    main()
