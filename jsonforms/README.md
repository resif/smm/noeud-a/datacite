# JSON Forms `Scientist Datacite` App

This app uses [JSON Forms](https://jsonforms.io) with React in order to render
a simple form for displaying a task entity.
It is based on `json-seed-app, which` is based on `create-react-app`.

- Execute `npm ci` to install the prerequisites. If you want to have the latest released versions use `npm install`.
- Execute `npm start` to start the application.

Browse to http://localhost:3000 to see the application in action.

## File Structure

The most important files:

- `src/scientist.datacite.schema.json` contains the JSON schema (also referred to as 'data schema')
	- Directly used Datacite fields are written in lowerCamelCase
	- Fields that will not go into datacite or need to be renamed/structured before going in are written in UpperCamelCase
	- Special fields for conversion to XML (COMMENT and VALUE) are written in UPPERCASE
- `src/scientist.datacite.uischema.json` contains the UI schema
- `src/index.tsx` is the entry point of the application.
  Customized the Material UI theme to give each control more space.
- `src/App.tsx` is the main app component and makes use of the `JsonForms`
  component to render a form.

The [data schema](src/scientist.datacite.schema.json) defines the structure
of a Task: it contains attributes such as title, description, due date and so on.

The [corresponding UI schema](src/scientist.datacite.uischema.json) specifies
controls for each property and puts them into a layout .

## Rendering JSON Forms

JSON Forms is rendered by importing and using the `JsonForms` component
and directly handing over the `schema`, `uischema`, `data`, `renderer` and
`cell` props.
Listen to changes in the form via the `onChange` callback.

## Custom renderers

Please see [the json forms tutorial](https://jsonforms.io/docs/tutorial) on
how to add custom renderers.
