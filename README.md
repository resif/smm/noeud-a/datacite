# DataCite

schemas and example files for creating datacite.  Steps are:

1. Create a "Scientist DataCite" JSON file using the JSON Forms web interface in `jsonforms\`
    - Name it `{network}.scientist.datacite.json`
1. Put `{network}.scientist.datacite.json` in `datacite_json/``
1. In `datacite_json`, run `datacite_json_to_xml.py` on the result:
    - Reads A-node and Data Center information (`smm.anode.xml`)
    - Reads scientist JSON form, restructures some for XML attributes
    - Blends the scientist information with the A-node/Data Center XML
    - Writes out the result as an XML file
1. Move the output XML file to `datacite_xml/`
1. Validate the result using `datacite_xml/validate_datacite.sh`
1. Submit resulting Datacite XML file to the Data Center
